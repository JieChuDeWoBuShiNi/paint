package com.immcque.paint.util

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import com.immcque.paint.R

/**
 *
 * @author  Lai
 *
 * @time 2021/3/8 0:52
 * @describe describe
 *
 */
object DialogUtils {
    fun showTip(context: Context, content: String, listener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(context).setTitle(context.getString(R.string.paint_model_tips_title)).setMessage(content)
            .setPositiveButton(context.getString(R.string.paint_model_positive_str), listener).setNegativeButton(context.getString(
                            R.string.paint_model_negative_str)) { d, _ ->
                d.dismiss()
            }.show()
    }

}