package com.immcque.paint.util;


import com.immcque.paint.R;

import java.io.Serializable;
import java.util.ArrayList;

public class TextColorUtil {
    public static class TextColor implements Serializable {

        public String color;
        public int drawable;

        public TextColor(String color, int drawable) {
            this.color = color;
            this.drawable = drawable;
        }
    }

    public static ArrayList<TextColor> getTextColors() {
        ArrayList<TextColor> textColors = new ArrayList<>();

        textColors.add(new TextColor("#ffffff", R.drawable.draw_ffffff));
        textColors.add(new TextColor("#000000", R.drawable.draw_000000));
        textColors.add(new TextColor("#4C4C4C", R.drawable.draw_4C4C4C));
        textColors.add(new TextColor("#727070", R.drawable.draw_727070));
        textColors.add(new TextColor("#AEADAD", R.drawable.draw_AEADAD));
        textColors.add(new TextColor("#CECECE", R.drawable.draw_CECECE));
        textColors.add(new TextColor("#0A9A8B", R.drawable.draw_0A9A8B));
        textColors.add(new TextColor("#0A9A55", R.drawable.draw_0A9A55));
        textColors.add(new TextColor("#47C187", R.drawable.draw_47C187));
        textColors.add(new TextColor("#6CFFBF", R.drawable.draw_6CFFBF));
        textColors.add(new TextColor("#90FF79", R.drawable.draw_90FF79));
        textColors.add(new TextColor("#EA4552", R.drawable.draw_EA4552));
        textColors.add(new TextColor("#FF6C6C", R.drawable.draw_FF6C6C));
        textColors.add(new TextColor("#FF9B6C", R.drawable.draw_FF9B6C));
        textColors.add(new TextColor("#F8E71C", R.drawable.draw_F8E71C));
        textColors.add(new TextColor("#FEFE34", R.drawable.draw_FEFE34));
        textColors.add(new TextColor("#A645EA", R.drawable.draw_A645EA));
        textColors.add(new TextColor("#EA45CB", R.drawable.draw_EA45CB));
        textColors.add(new TextColor("#EA4598", R.drawable.draw_EA4598));
        textColors.add(new TextColor("#FF79C6", R.drawable.draw_FF79C6));
        textColors.add(new TextColor("#FFACDC", R.drawable.draw_FFACDC));
        textColors.add(new TextColor("#41099A", R.drawable.draw_41099A));
        textColors.add(new TextColor("#0A719A", R.drawable.draw_0A719A));
        textColors.add(new TextColor("#6C75FF", R.drawable.draw_6C75FF));
        textColors.add(new TextColor("#6CB7FF", R.drawable.draw_6CB7FF));
        textColors.add(new TextColor("#79EFFF", R.drawable.draw_79EFFF));
        textColors.add(new TextColor("#0e1112", R.drawable.draw_0e1112));
        textColors.add(new TextColor("#f7527b", R.drawable.draw_f7527b));
        textColors.add(new TextColor("#9fb7c8", R.drawable.draw_9fb7c8));
        textColors.add(new TextColor("#1b9ff8", R.drawable.draw_1b9ff8));
        textColors.add(new TextColor("#5af9f8", R.drawable.draw_5af9f8));
        textColors.add(new TextColor("#32f32d", R.drawable.draw_32f32d));
        textColors.add(new TextColor("#fff657", R.drawable.draw_fff657));
        textColors.add(new TextColor("#fd9426", R.drawable.draw_fd9426));
        textColors.add(new TextColor("#b8860b", R.drawable.draw_b8860b));
        textColors.add(new TextColor("#a52a2a", R.drawable.draw_a52a2a));
        textColors.add(new TextColor("#8e388e", R.drawable.draw_8e388e));
        textColors.add(new TextColor("#8968cd", R.drawable.draw_8968cd));
        textColors.add(new TextColor("#7fffd4", R.drawable.draw_7fffd4));
        textColors.add(new TextColor("#737373", R.drawable.draw_737373));
        textColors.add(new TextColor("#3b3b3b", R.drawable.draw_3b3b3b));
        textColors.add(new TextColor("#00ff00", R.drawable.draw_7ed321));
        textColors.add(new TextColor("#68228b", R.drawable.draw_68228b));
        textColors.add(new TextColor("#ee2c2c", R.drawable.draw_ee2c2c));
        textColors.add(new TextColor("#ff8c00", R.drawable.draw_ff8c00));
        textColors.add(new TextColor("#ff00ff", R.drawable.draw_ff00ff));
        textColors.add(new TextColor("#ffd700", R.drawable.draw_ffd700));
        textColors.add(new TextColor("#262626", R.drawable.draw_262626));
        textColors.add(new TextColor("#00ee00", R.drawable.draw_29fa2f));
        textColors.add(new TextColor("#fff8dc", R.drawable.draw_fff8dc));
        textColors.add(new TextColor("#ffdead", R.drawable.draw_ffdead));
        textColors.add(new TextColor("#cdb5cd", R.drawable.draw_cdb5cd));
        textColors.add(new TextColor("#43cd80", R.drawable.draw_43cd80));
        textColors.add(new TextColor("#0d8d83", R.drawable.draw_0d8d83));
        textColors.add(new TextColor("#040001", R.drawable.draw_040001));
        textColors.add(new TextColor("#6f0000", R.drawable.draw_6f0000));
        return textColors;
    }

    public static ArrayList<TextColor> getAllColors() {
        ArrayList<TextColor> textColors = new ArrayList<>();

        textColors.add(new TextColor("#ffffff", R.drawable.draw_ffffff));
        textColors.add(new TextColor("#EBEBEB", R.drawable.draw_EBEBEB));
        textColors.add(new TextColor("#D6D6D6", R.drawable.draw_D6D6D6));
        textColors.add(new TextColor("#C2C2C2", R.drawable.draw_C2C2C2));
        textColors.add(new TextColor("#ADADAD", R.drawable.draw_ADADAD));
        textColors.add(new TextColor("#999999", R.drawable.draw_999999));
        textColors.add(new TextColor("#858585", R.drawable.draw_858585));
        textColors.add(new TextColor("#707070", R.drawable.draw_707070));
        textColors.add(new TextColor("#5C5C5C", R.drawable.draw_5C5C5C));
        textColors.add(new TextColor("#474747", R.drawable.draw_474747));
        textColors.add(new TextColor("#333333", R.drawable.draw_333333));
        textColors.add(new TextColor("#000000", R.drawable.draw_000000));

        textColors.add(new TextColor("#02374A", R.drawable.draw_02374A));
        textColors.add(new TextColor("#011D56", R.drawable.draw_011D56));
        textColors.add(new TextColor("#1A0A52", R.drawable.draw_1A0A52));
        textColors.add(new TextColor("#2E063C", R.drawable.draw_2E063C));
        textColors.add(new TextColor("#3B071B", R.drawable.draw_3B071B));
        textColors.add(new TextColor("#5C0701", R.drawable.draw_5C0701));
        textColors.add(new TextColor("#5A1C00", R.drawable.draw_5A1C00));
        textColors.add(new TextColor("#583300", R.drawable.draw_583300));
        textColors.add(new TextColor("#563D00", R.drawable.draw_563D00));
        textColors.add(new TextColor("#666100", R.drawable.draw_666100));
        textColors.add(new TextColor("#4F5504", R.drawable.draw_4F5504));
        textColors.add(new TextColor("#263E0F", R.drawable.draw_263E0F));

        textColors.add(new TextColor("#004D64", R.drawable.draw_004D64));
        textColors.add(new TextColor("#012F7B", R.drawable.draw_012F7B));
        textColors.add(new TextColor("#1A0A51", R.drawable.draw_1A0A51));
        textColors.add(new TextColor("#450D58", R.drawable.draw_450D58));
        textColors.add(new TextColor("#551029", R.drawable.draw_551029));
        textColors.add(new TextColor("#831100", R.drawable.draw_831100));
        textColors.add(new TextColor("#7A2900", R.drawable.draw_7A2900));
        textColors.add(new TextColor("#7A4A00", R.drawable.draw_7A4A00));
        textColors.add(new TextColor("#785800", R.drawable.draw_785800));
        textColors.add(new TextColor("#8C8602", R.drawable.draw_8C8602));
        textColors.add(new TextColor("#6F760A", R.drawable.draw_6F760A));
        textColors.add(new TextColor("#38571A", R.drawable.draw_38571A));

        textColors.add(new TextColor("#006E8E", R.drawable.draw_006E8E));
        textColors.add(new TextColor("#0042A8", R.drawable.draw_0042A8));
        textColors.add(new TextColor("#2C0977", R.drawable.draw_2C0977));
        textColors.add(new TextColor("#61187B", R.drawable.draw_61187B));
        textColors.add(new TextColor("#79193D", R.drawable.draw_79193D));
        textColors.add(new TextColor("#B51A00", R.drawable.draw_B51A00));
        textColors.add(new TextColor("#AD3D00", R.drawable.draw_AD3D00));
        textColors.add(new TextColor("#A96800", R.drawable.draw_A96800));
        textColors.add(new TextColor("#A57B02", R.drawable.draw_A57B02));
        textColors.add(new TextColor("#C3BC00", R.drawable.draw_C3BC00));
        textColors.add(new TextColor("#9BA40F", R.drawable.draw_9BA40F));
        textColors.add(new TextColor("#4E7A27", R.drawable.draw_4E7A27));

        textColors.add(new TextColor("#008CB4", R.drawable.draw_008CB4));
        textColors.add(new TextColor("#0255D6", R.drawable.draw_0255D6));
        textColors.add(new TextColor("#371994", R.drawable.draw_371994));
        textColors.add(new TextColor("#7A219E", R.drawable.draw_7A219E));
        textColors.add(new TextColor("#98244F", R.drawable.draw_98244F));
        textColors.add(new TextColor("#E12401", R.drawable.draw_E12401));
        textColors.add(new TextColor("#DA5100", R.drawable.draw_DA5100));
        textColors.add(new TextColor("#D28200", R.drawable.draw_D28200));
        textColors.add(new TextColor("#D19D02", R.drawable.draw_D19D02));
        textColors.add(new TextColor("#F4EC00", R.drawable.draw_F4EC00));
        textColors.add(new TextColor("#C2D017", R.drawable.draw_C2D017));
        textColors.add(new TextColor("#669D34", R.drawable.draw_669D34));

        textColors.add(new TextColor("#01A0D8", R.drawable.draw_01A0D8));
        textColors.add(new TextColor("#0161FD", R.drawable.draw_0161FD));
        textColors.add(new TextColor("#4D22B2", R.drawable.draw_4D22B2));
        textColors.add(new TextColor("#982BBB", R.drawable.draw_982BBB));
        textColors.add(new TextColor("#B92D5D", R.drawable.draw_B92D5D));
        textColors.add(new TextColor("#FF4015", R.drawable.draw_FF4015));
        textColors.add(new TextColor("#FF6A00", R.drawable.draw_FF6A00));
        textColors.add(new TextColor("#FEAA02", R.drawable.draw_FEAA02));
        textColors.add(new TextColor("#FCC700", R.drawable.draw_FCC700));
        textColors.add(new TextColor("#FDFB42", R.drawable.draw_FDFB42));
        textColors.add(new TextColor("#D8EC37", R.drawable.draw_D8EC37));
        textColors.add(new TextColor("#75BB3F", R.drawable.draw_75BB3F));

        textColors.add(new TextColor("#03C7FB", R.drawable.draw_03C7FB));
        textColors.add(new TextColor("#3A87FD", R.drawable.draw_3A87FD));
        textColors.add(new TextColor("#5E30EB", R.drawable.draw_5E30EB));
        textColors.add(new TextColor("#BE37F2", R.drawable.draw_BE37F2));
        textColors.add(new TextColor("#E63B7A", R.drawable.draw_E63B7A));
        textColors.add(new TextColor("#FE614F", R.drawable.draw_FE614F));
        textColors.add(new TextColor("#FF8648", R.drawable.draw_FF8648));
        textColors.add(new TextColor("#FEB43F", R.drawable.draw_FEB43F));
        textColors.add(new TextColor("#FECB3E", R.drawable.draw_FECB3E));
        textColors.add(new TextColor("#FEF76A", R.drawable.draw_FEF76A));
        textColors.add(new TextColor("#E4EE65", R.drawable.draw_E4EE65));
        textColors.add(new TextColor("#96D35F", R.drawable.draw_96D35F));

        textColors.add(new TextColor("#51D6FB", R.drawable.draw_51D6FB));
        textColors.add(new TextColor("#74A6FF", R.drawable.draw_74A6FF));
        textColors.add(new TextColor("#864EFD", R.drawable.draw_864EFD));
        textColors.add(new TextColor("#D357FD", R.drawable.draw_D357FD));
        textColors.add(new TextColor("#EE719E", R.drawable.draw_EE719E));
        textColors.add(new TextColor("#FE8C82", R.drawable.draw_FE8C82));
        textColors.add(new TextColor("#FFA57D", R.drawable.draw_FFA57D));
        textColors.add(new TextColor("#FFC777", R.drawable.draw_FFC777));
        textColors.add(new TextColor("#FED977", R.drawable.draw_FED977));
        textColors.add(new TextColor("#FFF993", R.drawable.draw_FFF993));
        textColors.add(new TextColor("#EAF18F", R.drawable.draw_EAF18F));
        textColors.add(new TextColor("#B1DC8A", R.drawable.draw_B1DC8A));

        textColors.add(new TextColor("#93E2FC", R.drawable.draw_93E2FC));
        textColors.add(new TextColor("#A7C6FF", R.drawable.draw_A7C6FF));
        textColors.add(new TextColor("#B18CFD", R.drawable.draw_B18CFD));
        textColors.add(new TextColor("#E191FD", R.drawable.draw_E191FD));
        textColors.add(new TextColor("#F4A4C0", R.drawable.draw_F4A4C0));
        textColors.add(new TextColor("#FFB5AF", R.drawable.draw_FFB5AF));
        textColors.add(new TextColor("#FFC5AB", R.drawable.draw_FFC5AB));
        textColors.add(new TextColor("#FED9A8", R.drawable.draw_FED9A8));
        textColors.add(new TextColor("#FDE3A8", R.drawable.draw_FDE3A8));
        textColors.add(new TextColor("#FFFBB9", R.drawable.draw_FFFBB9));
        textColors.add(new TextColor("#F1F6B7", R.drawable.draw_F1F6B7));
        textColors.add(new TextColor("#CDE8B4", R.drawable.draw_CDE8B4));

        textColors.add(new TextColor("#CBEFFF", R.drawable.draw_CBEFFF));
        textColors.add(new TextColor("#D2E1FF", R.drawable.draw_D2E1FF));
        textColors.add(new TextColor("#D9C8FE", R.drawable.draw_D9C8FE));
        textColors.add(new TextColor("#EFCAFF", R.drawable.draw_EFCAFF));
        textColors.add(new TextColor("#F8D3DF", R.drawable.draw_F8D3DF));
        textColors.add(new TextColor("#FFDBD8", R.drawable.draw_FFDBD8));
        textColors.add(new TextColor("#FFE2D5", R.drawable.draw_FFE2D5));
        textColors.add(new TextColor("#FEEBD3", R.drawable.draw_FEEBD3));
        textColors.add(new TextColor("#FEF2D5", R.drawable.draw_FEF2D5));
        textColors.add(new TextColor("#FDFCDD", R.drawable.draw_FDFCDD));
        textColors.add(new TextColor("#F7F9DB", R.drawable.draw_F7F9DB));
        textColors.add(new TextColor("#DEEED4", R.drawable.draw_DEEED4));
        return textColors;
    }

    public static ArrayList<TextColor> getNoteDownTxtColors() {
        ArrayList<TextColor> textColors = new ArrayList<>();

        textColors.add(new TextColor("#FF0000", R.drawable.draw_FF0000));
        textColors.add(new TextColor("#ffffff", R.drawable.draw_ffffff));
        textColors.add(new TextColor("#0091FF", R.drawable.draw_0091FF));
        textColors.add(new TextColor("#BD10E0", R.drawable.draw_BD10E0));
        textColors.add(new TextColor("#50E3C2", R.drawable.draw_50E3C2));
        textColors.add(new TextColor("#B8E986", R.drawable.draw_B8E986));
        textColors.add(new TextColor("#F8E71C", R.drawable.draw_F8E71C));
        textColors.add(new TextColor("#1CF847", R.drawable.draw_1CF847));

        return textColors;
    }
}
