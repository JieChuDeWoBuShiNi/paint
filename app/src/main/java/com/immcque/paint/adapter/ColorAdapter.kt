package com.immcque.paint.adapter

import android.graphics.Color
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.listener.OnItemClickListener
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.immcque.paint.R
import com.immcque.paint.util.TextColorUtil

/**
 */
class ColorAdapter : BaseQuickAdapter<TextColorUtil.TextColor, BaseViewHolder>(
    R.layout.paint_item_color,
    TextColorUtil.getTextColors()
) {


    var lastSelectColor = "#000000"
    var lastIndex = 1


    override fun convert(holder: BaseViewHolder, item: TextColorUtil.TextColor) {
        if (lastSelectColor == item.color) {
            holder.setBackgroundResource(R.id.rl_color, R.drawable.paint_color_select)
        } else {
            holder.setBackgroundColor(R.id.rl_color, Color.TRANSPARENT)
        }
        holder.setImageResource(R.id.iv_color, item.drawable)
    }


    override fun setOnItemClickListener(listener: OnItemClickListener?) {
        super.setOnItemClickListener { adapter, view, position ->
            lastSelectColor = data[position].color
            notifyItemChanged(lastIndex)
            notifyItemChanged(position)
            lastIndex = position
            listener?.onItemClick(adapter, view, position)
        }
    }

}