package com.immcque.paint.pen

import android.graphics.PointF
import android.view.MotionEvent
import android.view.View
import com.immcque.paint.pen.page.BrushInfoConfig
import com.immcque.paint.widget.WhiteboardTextureView
import java.util.ArrayDeque

/**
 * @author: Lai
 * @createDate: 2020-04-24 15:43
 * @description:
 */
interface IPen {
    fun downEvent(pagerView: View, coordinate:PointF,event: MotionEvent)
    fun moveEvent(pagerView: View, coordinate:PointF,event: MotionEvent)
    fun upEvent(pagerView: View, coordinate:PointF,event: MotionEvent)
    fun getDrawPoints(): ArrayList<Float>
    fun drawPoints(points: FloatArray, textureRotate: Float)

    fun rest()
    fun copy(brushInfoConfig: BrushInfoConfig): IPen
    fun getConfig(): BrushInfoConfig?

    fun fillDownEvent(pagerView: View, coordinate: PointF, event: MotionEvent)
    fun fillMoveEvent(pagerView: View, coordinate:PointF,event: MotionEvent)
    fun fillUpEvent(
        pagerView: View,
        coordinate: PointF,
        event: MotionEvent,
        mCurrBrushConfig: BrushInfoConfig?,
        mDraPenArrayDeque: ArrayDeque<IPen>,
        mIWhiteboardStatus: WhiteboardTextureView.IWhiteboardStatus?
    )
}