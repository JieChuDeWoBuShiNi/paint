package com.immcque.paint.pen.page.setting;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.immcque.paint.R;
import com.immcque.paint.adapter.BrushAdapter;
import com.immcque.paint.adapter.ColorAdapter;
import com.immcque.paint.pen.TexturePen;
import com.immcque.paint.pen.page.BrushInfoConfig;
import com.immcque.paint.pen.page.BrushManager;
import com.immcque.paint.pen.page.BrushRes;
import com.immcque.paint.util.TextColorUtil;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

public class PaintSettingFragment extends BaseFragment {
    private RecyclerView rvColors;
    private RecyclerView rvStyle;
    private IndicatorSeekBar sbSize;
    private IndicatorSeekBar sbAlpha;
    private BrushInfoConfig curBrushInfoConfig = BrushManager.INSTANCE.getDefaultBrushInfoConfig();
    private IBrushSetting brushSelect;
    private int alphaProgress;

    public PaintSettingFragment() {
    }

    public static PaintSettingFragment getInstance(IBrushSetting brushSelect) {
        PaintSettingFragment fragment = new PaintSettingFragment();
        fragment.setBrushSelect(brushSelect);
        return fragment;
    }

    private void setBrushSelect(IBrushSetting brushSelect) {
        this.brushSelect = brushSelect;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.paint_fragment_paint_setting;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        rvColors = (RecyclerView) view.findViewById(R.id.rv_colors);
        rvStyle = (RecyclerView) view.findViewById(R.id.rv_style);
        sbSize = (IndicatorSeekBar) view.findViewById(R.id.sb_size);
        sbAlpha = (IndicatorSeekBar) view.findViewById(R.id.sb_alpha);
        curBrushInfoConfig.setOutType(TexturePen.DRAW);

        rvColors.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        ColorAdapter colorAdapter = new ColorAdapter();
        colorAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
                TextColorUtil.TextColor item = colorAdapter.getItem(position);
                int color = Color.parseColor(item.color);
                int newColor = changeColorAlpha(alphaProgress, color);
                curBrushInfoConfig.setCurrColorInt(newColor);
                if (brushSelect != null) {
                    brushSelect.setColor(newColor);
                }
            }
        });
        rvColors.setAdapter(colorAdapter);

        rvStyle.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        BrushAdapter brushAdapter = new BrushAdapter();
        brushAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
                BrushRes item = brushAdapter.getItem(position);
                curBrushInfoConfig.setRes(item);
                if (brushSelect != null) {
                    brushSelect.selectBrush(curBrushInfoConfig);
                }
            }
        });
        rvStyle.setAdapter(brushAdapter);


        float brushWidth = curBrushInfoConfig.getBrushWidth();
        sbSize.setProgress(brushWidth);
        sbSize.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
                if (seekParams.fromUser) {
                    curBrushInfoConfig.setBrushWidth(seekParams.progress);
                    if (brushSelect != null) {
                        brushSelect.selectBrush(curBrushInfoConfig);
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar indicatorSeekBar) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar indicatorSeekBar) {

            }
        });

        alphaProgress = (int) (Color.alpha(curBrushInfoConfig.getCurrColorInt()) / 255f * 100);
        sbAlpha.setProgress(alphaProgress);
        sbAlpha.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
                if (seekParams.fromUser) {
                    int currColorInt = curBrushInfoConfig.getCurrColorInt();
                    alphaProgress = seekParams.progress;
                    int newColor = changeColorAlpha(alphaProgress, currColorInt);
                    curBrushInfoConfig.setCurrColorInt(newColor);
                    if (brushSelect != null) {
                        brushSelect.setColor(newColor);
                    }

                }
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar indicatorSeekBar) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar indicatorSeekBar) {

            }
        });
    }

    private int changeColorAlpha(int progress, int currColorInt) {
        int alpha = (int) (progress * 1f / 100 * 255);
        int red = Color.red(currColorInt);
        int green = Color.green(currColorInt);
        int blue = Color.blue(currColorInt);
        return Color.argb(alpha, red, green, blue);
    }
}
