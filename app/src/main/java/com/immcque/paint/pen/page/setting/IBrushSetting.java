package com.immcque.paint.pen.page.setting;

import com.immcque.paint.pen.page.BrushInfoConfig;

public interface IBrushSetting {
    void selectBrush(BrushInfoConfig info);
    void setColor(int colorInt);
}
