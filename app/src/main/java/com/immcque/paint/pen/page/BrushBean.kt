package com.immcque.paint.pen.page

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import com.immcque.paint.R
import com.immcque.paint.pen.TexturePen

/**
 *
 * 笔刷资源
 */
data class BrushRes(val id: String, val resBrush: Int, var isRotate: Boolean = true)

data class BrushInfoConfig(
    var res: BrushRes?,
    var brushWidth: Float,
    var outType: Int,
    var points: FloatArray? = null,
    var vertexCount: Int = 0,
    var currColorInt: Int = Color.BLACK
)

object BrushManager {

    fun getDefaultBrushInfoConfig(): BrushInfoConfig {
        return BrushInfoConfig(getDefaultBrushRes(), 15f, TexturePen.DRAW)
    }

    /**
     * 是否是同一个id
     */
    fun isAgainSetTexture(id: String?, info: BrushInfoConfig): BrushRes? {
        return if (id == info.res?.id) {
            null
        } else {
            info.res
        }
    }

    private fun getDefaultBrushRes(): BrushRes {
        return BrushRes("1", R.mipmap.paint_brush_paint_normal_128)
    }
    fun getFillBrushRes(): BrushRes {
        return BrushRes("6", R.mipmap.paint_brush_paint_fill_128)
    }

    fun getBrushList(): MutableList<BrushRes> {
        return mutableListOf(
            getDefaultBrushRes(),
            BrushRes("2", R.mipmap.paint_brush_paint_crayon2_128),
            BrushRes("3", R.mipmap.paint_brush_paint_marker_128, false),
            BrushRes("4", R.mipmap.paint_brush_paint_blur_128),
            BrushRes("5", R.mipmap.paint_brush_paint_crayon_128)
        )
    }

    fun getBrushBitmap(resources: Resources, brushRes: BrushRes?): Bitmap? {
        brushRes?.apply {
            return BitmapFactory.decodeResource(resources, brushRes.resBrush)
        }
        return null
    }


}
