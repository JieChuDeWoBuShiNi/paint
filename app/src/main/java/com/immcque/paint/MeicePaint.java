package com.immcque.paint;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.immcque.paint.pen.page.PaintMainActivity;

import java.lang.ref.WeakReference;

public final class MeicePaint {

    private final WeakReference<Activity> mContext;
    private final WeakReference<Fragment> mFragment;

    private MeicePaint(Activity activity) {
        this(activity, (Fragment)null);
    }

    private MeicePaint(Fragment fragment) {
        this(fragment.getActivity(), fragment);
    }

    private MeicePaint(Activity activity, Fragment fragment) {
        this.mContext = new WeakReference(activity);
        this.mFragment = new WeakReference(fragment);
    }

    public static MeicePaint from(Activity activity) {
        return new MeicePaint(activity);
    }

    public static MeicePaint from(Fragment fragment) {
        return new MeicePaint(fragment);
    }

    public static String obtainResult(Intent data) {
        return data.getStringExtra("extra_result_cache_path");
    }

    public void forResult(int requestCode) {
        Activity activity = this.getActivity();
        if (activity != null) {
            Intent intent = new Intent(activity, PaintMainActivity.class);
            Fragment fragment = this.getFragment();
            if (fragment != null) {
                fragment.startActivityForResult(intent, requestCode);
            } else {
                activity.startActivityForResult(intent, requestCode);
            }
        }
    }

    @Nullable
    Activity getActivity() {
        return (Activity)this.mContext.get();
    }

    @Nullable
    Fragment getFragment() {
        return this.mFragment != null ? (Fragment)this.mFragment.get() : null;
    }
}
