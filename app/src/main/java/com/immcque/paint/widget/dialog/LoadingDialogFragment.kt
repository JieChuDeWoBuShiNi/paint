package com.immcque.paint.widget.dialog

import android.os.Bundle
import android.view.View
import com.immcque.paint.R

/**
 *等待弹窗
 *
 */
class LoadingDialogFragment : BaseDialogFragment() {
    override fun getLayoutId(): Int {
        return R.layout.paint_dialog_loading
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
    }

    override fun isTouchClose(): Boolean {
        return false
    }

}